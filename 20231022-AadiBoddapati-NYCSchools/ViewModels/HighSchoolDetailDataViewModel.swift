//
//  HighSchoolDetailDataViewModel.swift
//  20231022-AadiBoddapati-NYCSchools
//
//  Created by Aadi on 10/22/23.
//

import Foundation

protocol HighSchoolDetailDataViewModelWrapper {
    var schoolDetailData: [HighSchoolDetailModel] { get set }
    func fetchSchoolData(params: [String: String])
}

class HighSchoolDetailDataViewModel: HighSchoolDetailDataViewModelWrapper {
    
    var updateUI: ((_ highSchools: [HighSchoolDetailModel], _ error: Error?) -> Void)?
    var schoolDetailData = [HighSchoolDetailModel]()
    
    func fetchSchoolData(params: [String: String]) {
        NetworkManager.shared.request(fromURL: AppURLs.highScoolDetailAPI, parms: params) { [weak self] (result: Result<[HighSchoolDetailModel], Error>) in
            guard let weakSelf = self else { return }
            switch result {
            case .success(let school):
                weakSelf.schoolDetailData = weakSelf.validateData(data: school)
                weakSelf.updateUI?(weakSelf.schoolDetailData, nil)
            case .failure(let error):
                weakSelf.schoolDetailData = [ HighSchoolDetailModel.dummyData ]
                weakSelf.updateUI?([], error)
            }
        }
    }
    
    private func validateData(data: [HighSchoolDetailModel]) -> [ HighSchoolDetailModel] {
        guard data.count > 0 else {
            return [ HighSchoolDetailModel.dummyData]
        }
        var detailModel = HighSchoolDetailModel.dummyData
        
        detailModel.dbn = data.first?.dbn ?? "0"
        detailModel.schoolName = data.first?.schoolName ?? "N/A"
        
        detailModel.numberOfTestTakers = validateMeanValue(meanValue: data.first?.numberOfTestTakers)
        detailModel.criticalReadingMean = validateMeanValue(meanValue: data.first?.criticalReadingMean)
        detailModel.mathematicsMean = validateMeanValue(meanValue: data.first?.mathematicsMean)
        detailModel.writingMean = validateMeanValue(meanValue: data.first?.writingMean)
        
        return [detailModel]
    }
    
    private func validateMeanValue(meanValue: String?) -> String {
        guard let nonNilValue = meanValue else { return "0"}
        guard let _ = Int(nonNilValue) else {
            return "0"
        }
        return nonNilValue
    }
}
