//
//  HighSchoolDataViewModel.swift
//  20231022-AadiBoddapati-NYCSchools
//
//  Created by Aadi on 10/22/23.
//

import Foundation

protocol HighSchoolDataViewModelWrapper {
    var schoolsData: [HighSchoolDataModel] { get set }
    func fetchData()
}

class HighSchoolDataViewModel: HighSchoolDataViewModelWrapper {
    var updateUI: ((_ highSchools: [HighSchoolDataModel], _ error: Error?) -> Void)?
    var schoolsData = [HighSchoolDataModel]()
    
    func fetchData() {
        // ["$$app_token": "7pz20kIyizkfXPOu31LOeD7Wv"]
        NetworkManager.shared.request(fromURL: AppURLs.highScoolsAPI, parms: nil) { [weak self] (result: Result<[HighSchoolDataModel], Error>) in
            switch result {
            case .success(let schools):
                self?.schoolsData = schools
                self?.updateUI?(schools, nil)
            case .failure(let error):
                self?.updateUI?([], error)
            }
        }
    }

    func getNumberOfItems() -> Int {
        return schoolsData.count
    }
    
    func getTitleFor(_ indexPath: IndexPath) -> String {
        guard schoolsData[indexPath.row].schoolName.count > 0 else {
            return ""
        }
        return schoolsData[indexPath.row].schoolName
    }
    
    func getDBNFor(_ indexPath: IndexPath) -> String {
        guard schoolsData[indexPath.row].dbn.count > 0 else {
            return ""
        }
        return schoolsData[indexPath.row].dbn
    }
}
