//
//  HighSchoolDetailModel.swift
//  20231022-AadiBoddapati-NYCSchools
//
//  Created by Aadi on 10/22/23.
//

import Foundation

struct HighSchoolDetailModel: Codable {
    var dbn: String
    var schoolName: String
    var numberOfTestTakers: String
    var criticalReadingMean: String
    var mathematicsMean: String
    var writingMean: String
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case criticalReadingMean = "sat_critical_reading_avg_score"
        case mathematicsMean = "sat_math_avg_score"
        case writingMean = "sat_writing_avg_score"
    }
}

extension HighSchoolDetailModel {
   static var dummyData: HighSchoolDetailModel {
        HighSchoolDetailModel(dbn: "",
                              schoolName: "N/A",
                              numberOfTestTakers: "0",
                              criticalReadingMean: "0",
                              mathematicsMean: "0",
                              writingMean: "0")
    }
}
