//
//  HighSchoolDataModel.swift
//  20231022-AadiBoddapati-NYCSchools
//
//  Created by Aadi on 10/22/23.
//

import Foundation

struct HighSchoolDataModel: Codable {
    let dbn: String
    let schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
    }
}
