//
//  NetworkManager.swift
//  20231022-AadiBoddapati-NYCSchools
//
//  Created by Aadi on 10/22/23.
//

import Foundation

enum API {
    static let BaseURL = "https://data.cityofnewyork.us/resource/"
}
enum AppURLs {
    static let highScoolsAPI = "s3k6-pzi2.json"
    static let highScoolDetailAPI = "f9bf-2cp4.json"
}
enum APIError: Error {
    case invalidResponse
    case invalidStatusCode(Int)
}

enum HttpMethod: String {
    case post
    case get
    case put
    case delete
    
    func getRawValue() -> String {
        return self.rawValue.uppercased()
    }
}

///  NetworkManager singleton class is resonsible for making RESTFul api's
class NetworkManager {
    static let shared: NetworkManager = NetworkManager()
    private init() {}
    
    /// Using NetworkManager class call the API to fetch the data
    ///
    /// - Parameter fromURL: Required URL with string format to send the request
    /// - Parameter httpMethod: type of HTTP method(get, post, delete...)
    /// - Parameter parms: Used to pass any query parameters available
    /// - Parameter completion: an escaping clousere returns a data model or error
    func request<T: Decodable>(fromURL: String, httpMethod: HttpMethod = .get, parms: [String: String]?,completion: @escaping ( (Result<T, Error>) -> Void)) {
        guard var url = URL(string: API.BaseURL + fromURL) else { return }
        
        // Append Params if available
        if let queryParms = parms {
            url.append(queryItems: queryParms.map { element in URLQueryItem(name: element.key, value: element.value) })
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.getRawValue()
//        request.allHTTPHeaderFields = ["Accept": "application/json", "X-App-Token": "7pz20kIyizkfXPOu31LOeD7Wv"]
        
        let session = URLSession(configuration: .default)
        session.dataTask(with: request) { rawData, response, error in
            if let responseError = error {
                completion(.failure(responseError))
            }
            
            guard let urlResponse = response as? HTTPURLResponse else { return completion(.failure(APIError.invalidResponse)) }
            if !(200..<300).contains(urlResponse.statusCode) {
                completion(.failure(APIError.invalidStatusCode(urlResponse.statusCode)))
                return
            }
            
            guard let data = rawData else { return }
            do {
                let modelData = try JSONDecoder().decode(T.self, from: data)
                completion(.success(modelData))
            } catch {
                completion(.failure(error))
            }
            
        }.resume()
        
    }
    
}
