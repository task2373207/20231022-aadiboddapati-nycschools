//
//  Utility.swift
//  20231022-AadiBoddapati-NYCSchools
//
//  Created by Aadi on 10/22/23.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(_ message: String) {
        let cntrl = UIAlertController(title: "OOPS!", message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { action in
            
        }
        cntrl.addAction(okayAction)
        self.present(cntrl, animated: true)
    }
    
}
