//
//  HighSchoolViewModelTest.swift
//  20231022-AadiBoddapati-NYCSchoolsTests
//
//  Created by Aadi on 10/22/23.
//

import XCTest
@testable import _0231022_AadiBoddapati_NYCSchools

final class HighSchoolViewModelTest: XCTestCase {

    var sut: HighSchoolDataViewModel!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = HighSchoolDataViewModel()
        getHighSchoolsData()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut.schoolsData = []
        sut = nil
    }
    
    func testHighSchoolsData() {
        XCTAssert(sut.schoolsData.count > 0, "No data to display")
    }
    
    func testHighSchoolName() {
        XCTAssertEqual(sut.schoolsData.first?.schoolName, "Liberation Diploma Plus High School")
    }
    func testHighSchoolDBN() {
        XCTAssertEqual(sut.schoolsData.first?.dbn, "21K728")
    }
    
    private func getHighSchoolsData() {
        guard let aData = getDataFor(resource: "HighSchools"), let highScools = try? JSONDecoder().decode([HighSchoolDataModel].self, from: aData) else {
            XCTFail("Failed to read data")
            return
        }
        sut.schoolsData = highScools
    }

    private func getDataFor(resource name: String) -> Data? {
        if let path = Bundle.main.path(forResource: name, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch let error {
                print(error.localizedDescription)
                return nil
            }
        }
        return nil
    }

}
